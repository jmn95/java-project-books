import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Book implements Comparable<Book> {
    public String strBookName;
    public String strFileName;
    public int intNumberOfline = 0;

    List<Word> lWords = new ArrayList<Word>() ;


    Map<String, Long> mfreq = null ;

    Book(String strNewBook, String strNewFile) {
        strFileName = strNewFile;
        strBookName = strNewBook;

        List<String> Words = new ArrayList<String>() ;
       Pattern p = Pattern.compile("\\w+", Pattern.UNICODE_CHARACTER_CLASS);

        try (Scanner sc = new Scanner(new File(strNewFile))) {
            for (intNumberOfline = 0; sc.hasNextLine(); ++intNumberOfline) {
                for (Matcher m1 = p.matcher(sc.nextLine()); m1.find(); ) {
                    Words.add(m1.group().toLowerCase());
                }
            }
            mfreq = Words.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

            for(Map.Entry<String, Long> entry : mfreq.entrySet()) {
                lWords.add(new Word(entry.getKey(),entry.getValue()))  ;
            }
            Collections.sort(lWords);

        } catch (FileNotFoundException e) {
            System.out.println("Ce fichier n'existe pas :" + strNewFile);;
        }
    }

    public String toString() {
        return ("Livre : " + strBookName + " Chemin : " + strFileName) ;
    }

    public String details() {
        return ("Livre : " + strBookName + " nb ligne : " + intNumberOfline) ;
    }

     public String toStringTop (){
        String strResult = "" ;
        int intCount = 0 ;
        Word oCurrentWord ;
        ListIterator<Word> liWords = lWords.listIterator();

        while (liWords.hasNext() && intCount < 10) {
            oCurrentWord = liWords.next();
            strResult += oCurrentWord.strWord + " " + oCurrentWord.lngOccurence + "\n" ;
            intCount += 1;
        }


        return (strResult) ;
    }

    public boolean equals(Book oOtherBook) {
        return (this.strFileName.equals(oOtherBook.strFileName)) ;
    }

    @Override
    public int compareTo(Book oOtherBook) {
        int intResult = 0 ;

        intResult = this.strBookName.compareTo(oOtherBook.strBookName) ;
        if(intResult == 0 ) {
            intResult = this.strFileName.compareTo(oOtherBook.strFileName) ;
        }
        return (intResult) ;
    }
}
