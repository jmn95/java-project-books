import java.util.*;
import java.util.function.Function;

public class Main {

    public static void main(String[] args) {
        int intCurrentMenu = 0;
        String strSelection = "0" ;
        Menu oNewMenu = null;
        Menu oBookMenu = new Menu();
        BookCollection oBookCollection = new BookCollection();
        Book oCurrentBook = null ;
        List<Menu> lMenu = new ArrayList<Menu>();

        // Menu 0
        oNewMenu = new Menu();
        oNewMenu.additem("Lister les fichiers", "1");
        oNewMenu.additem("Ajouter un fichier", "2");
        oNewMenu.additem("Supprimer un fichier", "3");
        oNewMenu.additem("Afficher des informations sur un livre", "4");
        oNewMenu.additem("Quitter", "5");
        lMenu.add(oNewMenu);

        // Menu 1
        oNewMenu = new Menu();
        oNewMenu.additem("Sélectionner un fichier", "1");
        oNewMenu.additem("Afficher le nombre de lignes du fichier", "2");
        oNewMenu.additem("Afficher le nombre de mots du fichier ", "3");
        oNewMenu.additem("Retour menu précédent", "4");
        lMenu.add(oNewMenu);

        // Liste des livres
        lMenu.add(oBookMenu) ;

        // Chargement des fichiers si il existe
        if (args.length != 0) {
            for (int intIndex = 0; intIndex <= args.length - 1; ++intIndex) {
                oBookCollection.add(args[intIndex]);
            }
        } else {
            System.out.println("Pas de fichier en entrée");
        }

        intCurrentMenu = 0;
        strSelection = "0";
        do {
            System.out.println(lMenu.get(intCurrentMenu));
            try {
                strSelection = lMenu.get(intCurrentMenu).getselection();

                switch (intCurrentMenu) {
                    case 0:
                        intCurrentMenu = actionMenu0(strSelection, intCurrentMenu, oBookCollection);
                        break;
                    case 1:
                        intCurrentMenu = actionMenu1(strSelection, intCurrentMenu, oBookCollection, oCurrentBook, oBookMenu);
                        break;
                    case 2:
                        oCurrentBook = oBookCollection.getBook(strSelection);
                        intCurrentMenu = 1;
                        break;
                }
            } catch (SelectionException exception) {
                System.out.println(exception.getMessage());
                strSelection = "0";
            }

        } while ( !strSelection.equals("5") || intCurrentMenu != 0) ;
       return;
    }

    public static int actionMenu0(String strSelection, int intCurrentMenu, BookCollection oBookCollection) {
        int intNextMenu = intCurrentMenu ;
        switch (strSelection) {
            case "1":
                // 1. Lister les fichiers
                System.out.println(oBookCollection);
                break;
            case "2":
                // 2. Ajouter un fichier
                Scanner scKeyboard = new Scanner(System.in);
                System.out.print("Le chemin du fichier : ");
                oBookCollection.add(scKeyboard.nextLine());
                break;
            case "3":
                // 3. Supprimer un fichier
                Menu oDeleteMenu = new Menu() ;
                oBookCollection.menu(oDeleteMenu) ;
                if(!oDeleteMenu.isEmpty()) {
                    oDeleteMenu.additem("Pour annuler la suppression", "0");
                    System.out.println(oDeleteMenu);
                    try {
                        String strDeleteSelection = oDeleteMenu.getselection();
                        if (!strDeleteSelection.equals("0")) oBookCollection.remove(strDeleteSelection);
                    } catch (SelectionException exception) {
                        System.out.println(exception.getMessage());
                    }
                } else {
                    System.out.println("Aucun fichier dans la liste1" );
                }

                break;
            case "4":
                // 4. Afficher des informations sur un livre  / Changement de menu
                intNextMenu = 1;
                break;
            case "5":
                // 5. Books
                intNextMenu = 0;
                break;
        }
        return (intNextMenu) ;
    }

    public static int actionMenu1(String strSelection, int intCurrentMenu, BookCollection oBookCollection,Book oCurrentBook, Menu oBookMenu ) {
        int intNextMenu = intCurrentMenu ;

        switch (strSelection) {
            case "1":
                // 1. Sélectionner un fichier
                oBookMenu.clear();
                oBookCollection.menu(oBookMenu) ;
                intNextMenu = 2 ;
                break;
            case "2":
                // 2. Afficher le nombre de lignes du fichier
               if(oCurrentBook != null ) System.out.println(oCurrentBook.details());
                break;
            case "3":
                // 3. Afficher le nombre de mots du fichier
                if(oCurrentBook != null ) System.out.println(oCurrentBook.toStringTop());
                if(oCurrentBook != null ) System.out.println(oBookCollection.toUniqueWord(oCurrentBook));
                break;
            case "4":
                // 4. Retour au menu précédent
                intNextMenu = 0;
                break;
        }
        return (intNextMenu) ;
    }
}
