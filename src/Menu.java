import java.io.IOException;
import java.util.*;


public class Menu {

    Map<String, String> oMap = null ;

    Menu() throws NullPointerException {
        oMap = new HashMap<String, String>();
    }

    private boolean exist(String strKey) {
        boolean booResult = false ;

        for (Map.Entry<String,String> entry : oMap.entrySet())
            if(entry.getKey().equals(strKey)) booResult = true ;

        return(booResult) ;
    }

    public void additem(String strNewItem,String strNewKey) {
        oMap.put(strNewKey, strNewItem);
    }

    public void clear() {
        oMap.clear();
    }


    public String toString (){
        String strResult = "" ;
        SortedSet<String> keys = new TreeSet<>(oMap.keySet());
        for (String key : keys) {
            strResult += key + ": " + oMap.get(key) + "\n";
        }
        return (strResult) ;
    }

    public String getselection() throws SelectionException {
        String strResult = null ;

        Scanner scKeyboard = new Scanner(System.in);
            System.out.print("Saisissez un numéro : ");
            strResult = scKeyboard.nextLine();
            if (this.exist(strResult)) {
                return (strResult);
            } else {
                throw new SelectionException("Option incorrecte !");
            }


    }

    public boolean isEmpty() {
        return (oMap.isEmpty());
    }
}
