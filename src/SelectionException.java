public class SelectionException extends RuntimeException {
    public SelectionException() {
        super();
    }

    public SelectionException(String s) {
        super(s);
    }
}
