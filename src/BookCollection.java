import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class BookCollection {

    List<Book> lBooks = null;

    BookCollection() {
        lBooks = new ArrayList<Book>();
    }

    public String toString() {
        Book oCurrentBook;
        String strResult = "";
        int intCount = 1;
        ListIterator<Book> liBooks = lBooks.listIterator();

        System.out.println("Liste des Livres");
        while (liBooks.hasNext()) {
            oCurrentBook = liBooks.next();
            strResult += Integer.toString(intCount) + " " + oCurrentBook.toString() + "\n";
            intCount += 1;
        }
        return (strResult);
    }

    public Book getBook(String strKey) {
        return (lBooks.get(Integer.parseInt(strKey) - 1));
    }


    public void menu(Menu oNewMenu) {
        Book oCurrentBook;
        String strResult = "";
        int intCount = 1;
        ListIterator<Book> liBooks = lBooks.listIterator();

        System.out.println("Liste des Livres");
        while (liBooks.hasNext()) {
            oCurrentBook = liBooks.next();
            oNewMenu.additem(oCurrentBook.toString(), Integer.toString(intCount));
            intCount += 1;
        }
    }

    public void add(String strNewItem) {
        File fNewFileName = null;

        fNewFileName = new File(strNewItem);
        if (fNewFileName.exists()) {
            if (fNewFileName.isDirectory()) {
                FilenameFilter filter = new FilenameFilter() {
                    public boolean accept(File fNewDirectory, String name) {
                        return name.endsWith(".txt");
                    }
                };

                File[] faNewFile = fNewFileName.listFiles(filter);
                for (File fNewFile : faNewFile) {
                    lBooks.add(new Book(fNewFile.getName(), fNewFile.getAbsolutePath()));
                }
                Collections.sort(lBooks);

            } else {
                lBooks.add(new Book(fNewFileName.getName(), fNewFileName.getAbsolutePath()));
            }
        } else {
            System.out.println("Ce fichier n'existe pas : " + strNewItem);
        }
    }

    public void remove(String strKey) {
        lBooks.remove(Integer.parseInt(strKey) - 1);
    }

    public List<Word> UniqueWord(Book oTestBook) {
        List<Word> lResult = new ArrayList<Word>();

        lResult.addAll(oTestBook.lWords) ;
        List<Word> lTemp =  null;

        for (Book oCurrentBook : lBooks) {
            if(! oCurrentBook.equals(oTestBook)) {
                for (Word oCurrentWord : oCurrentBook.lWords) {
                    lTemp = new ArrayList<Word>();
                    lTemp.addAll(lResult);
                    for (Word oTestWord : lTemp)
                        if (oCurrentWord.equals(oTestWord)) {
                            lResult.remove(oTestWord);
                        }
                }
            }
        }
        return (lResult) ;
    }

    public String toUniqueWord(Book oTestBook) {
        String strResult = "" ;

        List<Word> lResult = UniqueWord(oTestBook);

        for(Word oCurrentWord : lResult) {
            strResult += oCurrentWord.strWord + "\n" ;
        }
        return (strResult) ;


    }



    public String Pourcent(Book oTestBook) {
        String strResult = "" ;

        List<Word> lResult = new ArrayList<Word>();

        lResult.addAll(oTestBook.lWords) ;
        List<Word> lTemp =  null;

        for (Book oCurrentBook : lBooks) {
            if(! oCurrentBook.equals(oTestBook)) {
                for (Word oCurrentWord : oCurrentBook.lWords) {
                    lTemp = new ArrayList<Word>();
                    lTemp.addAll(lResult);
                    for (Word oTestWord : lTemp)
                        if (oCurrentWord.equals(oTestWord)) {
                            lResult.remove(oTestWord);
                        }
                }
            }
        }
        for(Word oCurrentWord : lResult) {
            strResult += oCurrentWord.strWord + "\n" ;
        }
        return (strResult) ;
    }
}

