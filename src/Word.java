public class Word  implements Comparable<Word> {
    String strWord = null ;
    Long lngOccurence = 0L ;
    Word(String strWord, Long lngOccurence) {
        this.strWord = strWord ;
        this.lngOccurence = lngOccurence ;
    }

    public boolean equals(Word oOtherWord) {
        return (this.strWord.equals(oOtherWord.strWord)) ;
    }

    @Override
    public int compareTo(Word oOtherWord) {
        int intResult = 0 ;

        intResult = this.lngOccurence.compareTo(oOtherWord.lngOccurence)*-1 ;
        if(intResult == 0 ) {
            intResult = this.strWord.compareTo(oOtherWord.strWord) ;
        }
        return (intResult) ;
    }
}
