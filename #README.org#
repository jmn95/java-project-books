# -*- mode: org; org-confirm-babel-evaluate: nil; org-babel-noweb-wrap-start: "«"; org-babel-noweb-wrap-end: "»"; ispell-local-dictionary: "fr_FR";-*-

#+TITLE: Mini-projet d'initiation à Java
#+AUTHOR: Jean-Marc Noël

#+LANGUAGE: fr
#+LANG: fr
#+HTML_HEAD_EXTRA: <style>*{font-family: monospace !important}</style>

#+BEGIN_SRC elisp :exports none :results silent
 (setq org-ditaa-jar-path "/usr/share/ditaa/ditaa.jar")
(org-babel-do-load-languages
 'org-babel-load-languages
 '((ditaa . t)
   (python . t)))
#+END_SRC

* Contexte

  Le programme doit permettre de travailler sur un ensemble de fichiers
texte correspondant à des livres.

* Installation

  L'archive est dans le répertoire ./out/artifacts/java\_project\_books\_jar .

* Menu
** Lister les fichiers
** Ajouter un fichier
** Supprimer un fichier
** Afficher des informations sur un livre
*** Afficher le nombre de lignes du fichier
*** Afficher le nombre de mots du fichier
** Quitter le programme
* Utilisation
** Chargement

  Le programme accepte de 0 à n arguments qui sont :
#+BEGIN_VERBATIM
1. Des fichiers 
-> 0 fichier => OK  
-> n fichier => OK
-> 1 répertoire => ok
-> pas un fichier => ok 
-> droit en lecture ?
#+END_VERBATIM


Pendant toute l'exécution du programme, celui-ci maintient une liste
des noms de fichiers, initialisée par les arguments du programme,
qu'il est possible de consulter et modifier avec les trois premières
options.

Le quatrième choix :
1. affiche la liste des fichiers
2. propose de choisir d'un de ces fichiers
3. propose le sous-menu suivant :
#+BEGIN_VERBATIM
1. Afficher le nombre de lignes du fichier
2. Afficher le nombre de mots du fichier
#+END_VERBATIM

* Objectif

Parmi les informations proposées pour un fichier, ajouter :
- afficher les 50 mots les plus fréquents et leur nombre d'occurrences
- afficher les mots qui sont présents seulement dans ce fichier et
  aucun des autres fichiers
- Afficher pour chacun des autres fichiers le pourcentage de mots de
  l'autre fichier qui sont présents dans le fichier sélectionnés, par
  ordre décroissant de ce pourcentage.

* Prétraitements

Les fichiers de texte contiennent, en plus des mots, des signes,
notamment de ponctuation, qu'on voudra éliminer. Pour faciliter le
mini-projet, on peut le faire avec un prétraitement, par exemple en
utilisant le programme suivant pour générer des fichiers ne contenant
que les mots (un par ligne), d'un fichier texte :


#+BEGIN_SRC java :exports code :tangle BooksToWords.java
import java.io.FileNotFoundException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BooksToWords {
    public static void main(String[] args)throws FileNotFoundException {
        Pattern p = Pattern.compile("\\w+", Pattern.UNICODE_CHARACTER_CLASS);
        try(Scanner sc = new Scanner(new File(args[0]));
            PrintStream fileOut = new PrintStream(new FileOutputStream(args[1]))){
            for(int i=0; sc.hasNextLine(); ++i){
                for(Matcher m1 = p.matcher(sc.nextLine()); m1.find();) {
                    fileOut.println(m1.group());
                }
            }
        }
    }
}
#+END_SRC

On peut aussi réaliser ce programme en python :

#+BEGIN_SRC python :exports code :tangle book-to-words :shebang "#!/usr/bin/env python3"
import sys
import re
with open(sys.argv[1]) as input, open(sys.argv[2], "w") as output:
    for line in input:
        for word in re.findall(r'\w+', line):
            output.write(word.lower())
            output.write('\n')
#+END_SRC

Si vous le voulez, vous pouvez vous inspirer de ce code pour que votre
programme puisse traiter directement les fichiers de texte en
réalisant vous-même l'extraction des mots.
