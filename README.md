
# Table des matières

1.  [Contexte](#orgc2fa7fb)
2.  [Installation](#orgd7ec253)
3.  [Menu](#org25e2ebe)
    1.  [Lister les fichiers](#orgd3dba3f)
    2.  [Ajouter un fichier](#org913a259)
    3.  [Supprimer un fichier](#org87e27ef)
    4.  [Afficher des informations sur un livre](#org27da9d2)
        1.  [Afficher le nombre de lignes du fichier](#orgb9d4786)
        2.  [Afficher le nombre de mots du fichier](#orgcc45486)
    5.  [Quitter le programme](#orgdc1cff2)
4.  [Utilisation](#orga8ddef2)
    1.  [Chargement](#org09171f4)
5.  [Objectif](#org6ba1184)
6.  [Prétraitements](#org9660af4)



<a id="orgc2fa7fb"></a>

# Contexte

  Le programme doit permettre de travailler sur un ensemble de fichiers
texte correspondant à des livres.


<a id="orgd7ec253"></a>

# Installation

L'archive est dans le répertoire ./out/artifacts/java\\<sub>project</sub>\\<sub>books</sub>\\<sub>jar</sub> .


<a id="org25e2ebe"></a>

# Menu


<a id="orgd3dba3f"></a>

## Lister les fichiers


<a id="org913a259"></a>

## Ajouter un fichier


<a id="org87e27ef"></a>

## Supprimer un fichier


<a id="org27da9d2"></a>

## Afficher des informations sur un livre


<a id="orgb9d4786"></a>

### Afficher le nombre de lignes du fichier


<a id="orgcc45486"></a>

### Afficher le nombre de mots du fichier


<a id="orgdc1cff2"></a>

## Quitter le programme


<a id="orga8ddef2"></a>

# Utilisation


<a id="org09171f4"></a>

## Chargement

Le programme accepte de 0 à n arguments qui sont :

<div class="VERBATIM">
1.  Des fichiers

-> 0 fichier => OK  
-> n fichier => OK
-> 1 répertoire
-> pas un fichier

1.  Des répertoires

</div>

Pendant toute l'exécution du programme, celui-ci maintient une liste
des noms de fichiers, initialisée par les arguments du programme,
qu'il est possible de consulter et modifier avec les trois premières
options.

Le quatrième choix :

1.  affiche la liste des fichiers
2.  propose de choisir d'un de ODces fichiers
3.  propose le sous-menu suivant :

<div class="VERBATIM">
1.  Afficher le nombre de lignes du fichier
2.  Afficher le nombre de mots du fichier

</div>


<a id="org6ba1184"></a>

# Objectif

Parmi les informations proposées pour un fichier, ajouter :

-   afficher les 50 mots les plus fréquents et leur nombre d'occurrences
-   afficher les mots qui sont présents seulement dans ce fichier et
    aucun des autres fichiers
-   Afficher pour chacun des autres fichiers le pourcentage de mots de
    l'autre fichier qui sont présents dans le fichier sélectionnés, par
    ordre décroissant de ce pourcentage.


<a id="org9660af4"></a>

# Prétraitements

Les fichiers de texte contiennent, en plus des mots, des signes,
notamment de ponctuation, qu'on voudra éliminer. Pour faciliter le
mini-projet, on peut le faire avec un prétraitement, par exemple en
utilisant le programme suivant pour générer des fichiers ne contenant
que les mots (un par ligne), d'un fichier texte :

    import java.io.FileNotFoundException;
    import java.io.File;
    import java.io.FileOutputStream;
    import java.io.PrintStream;
    import java.util.Scanner;
    import java.util.regex.Matcher;
    import java.util.regex.Pattern;
    
    public class BooksToWords {
        public static void main(String[] args)throws FileNotFoundException {
    	Pattern p = Pattern.compile("\\w+", Pattern.UNICODE_CHARACTER_CLASS);
    	try(Scanner sc = new Scanner(new File(args[0]));
    	    PrintStream fileOut = new PrintStream(new FileOutputStream(args[1]))){
    	    for(int i=0; sc.hasNextLine(); ++i){
    		for(Matcher m1 = p.matcher(sc.nextLine()); m1.find();) {
    		    fileOut.println(m1.group());
    		}
    	    }
    	}
        }
    }

On peut aussi réaliser ce programme en python :

    import sys
    import re
    with open(sys.argv[1]) as input, open(sys.argv[2], "w") as output:
        for line in input:
    	for word in re.findall(r'\w+', line):
    	    output.write(word.lower())
    	    output.write('\n')

Si vous le voulez, vous pouvez vous inspirer de ce code pour que votre
programme puisse traiter directement les fichiers de texte en
réalisant vous-même l'extraction des mots.

